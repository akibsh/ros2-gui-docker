# Git
```
git init
git add .
git commit -m "first commit"
git remote add origin https://github.com/namtranvn/ros2-docker-gui.git
git push --set-upstream origin master
```


# ROS2 Gazebo
```
source /opt/ros/humble/setup.bash
ign gazebo tugbot_depot.sdf
ign topic -t "/model/tugbot/cmd_vel" -m ignition.msgs.Twist -p "linear: {x: 1.0}"
ros2 run ros_ign_bridge parameter_bridge /model/tugbot/cmd_vel@geometry_msgs/msg/Twist@ignition.msgs.Twist
ros2 topic pub --once /model/tugbot/cmd_vel geometry_msgs/msg/Twist "{linear: {x: 1.0}}"
```